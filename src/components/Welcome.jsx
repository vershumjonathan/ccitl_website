import React from "react";

function Welcome() {
    return (
        <div className="d-flex container my-5 p-5 welcome-div">
            <div className="m-auto col-md-6">
                <h1 className="text-center display-3 pb-4 border-bottom mb-5">Welcome</h1>
                <p className="welcome-text">Calvary Chapel Into the Light is a place where people can meet Jesus, engage in
                    life-giving community, and everyone is welcome. We believe in creating a space where people can have authentic encounters
                    with Christ, discover their gifts and use them for God's glory. Join us for our services!
                </p>
            </div>
        </div>

    )
}

export default Welcome;