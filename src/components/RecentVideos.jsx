import React from "react";
import video from "../assets/Untitled.mp4"

function RecentVideos (){
    return(
        <div id="videoDiv2">
        <video id="video2" preload={true} autoPlay={true} muted={true} playsInline={true} loop={true} src={video} type="video/mp4">
        </video>
        <div id="videoMessage2" className="styling">
            <h1 className="text-center display-3 border-bottom mb-5">Recent Sermons</h1>
            <div className="row" style={{width:"100%"}}>
                <button className="btn btn-light video-btn col m-5">Sunday</button>
                <button className="btn btn-light video-btn col m-5">Wednesday</button>
            </div>

        </div>
    </div>
    )
}

export default RecentVideos;