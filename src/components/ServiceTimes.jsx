import React from "react";
import whiteDove from "../assets/dove_white.png"

function ServiceTimes(){
    return(
        <div className="m-0 p-4 service_times text-white row container-fluid">
        <div className="text-center col-lg my-auto p-4">
            <img src={whiteDove} className="p-4 welcome_dove" alt="" />
            <h1 className="display-3 text-shadow">Calvary Chapel <br/> Into The Light</h1>
            <p>"But if we walk in the light, as He is in the light, we have fellowship with one another,and the blood
                of Jesus His Son cleanses us from all sin." <br/> -1 John 1</p>
        </div>
        <div className="col-lg my-auto">
            <div className="pt-4">
                <h1 className="display-3 text-center text-shadow display-text-shadow">Service Times</h1>
            </div>
            <div className="d-flex justify-content-center">
                <div className="text-center p-lg-4 m-4 ">
                    <h2 className="display-6 border-bottom ">Sunday</h2>
                    <div>
                        <h3 className="display-6 ">8:30 am</h3>
                        <h3 className="display-6 ">10:30am</h3>
                    </div>
                </div>
                <div className="text-center p-lg-4 m-4 align-items-center justify-content-center">
                    <h2 className="display-6 border-bottom">Wednesday</h2>
                    <h2 className="display-6 pt-4"> 7:00 pm</h2>
                </div>
            </div>
        </div>
    </div>
    )
}

export default ServiceTimes;