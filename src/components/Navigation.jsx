import React from "react";
import { NavLink } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import navDove from "../assets/dove_black.png"


function Navigation() {
  return (

    <Navbar collapseOnSelect sticky="top" expand="md" bg="white" variant="light" >
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          <p className="m-0">
          <img className="nav-dove justify-content-center" src={navDove} alt=""/> CCITL
          </p>
        </NavLink>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse className="justify-content-end" id="responsive-navbar-nav">
          <div className="p-3-md text-center">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item mx-2 my-auto">
                <NavLink eventKey={1} className="nav-link" to="/">
                  Home
                </NavLink>
              </li>
              <li className="nav-item mx-2 my-auto">
                <NavLink eventKey={2} className="nav-link" to="/about">
                  About
                </NavLink>
              </li>
              <li className="nav-item mx-2 my-auto">
                <NavLink eventKey={3} className="nav-link" to="/teaching">
                  Teaching
                </NavLink>
              </li>
              <li className="nav-item mx-2 my-auto">
                <NavLink eventKey={4} className="nav-link" to="/give">
                  Give
                </NavLink>
              </li>
              <li className="nav-item mx-2 my-auto">
                <NavLink eventKey={5} className="nav-link" to="/connect">
                  Connect
                </NavLink>
              </li>
              <li class="nav-item mx-2">
                        <a class="nav-link active text-center" aria-current="page" href="watch_live.html"><button
                                type="button" class="btn btn-dark border border-rounded  round-button">Watch
                                Live</button></a>
                    </li>
            </ul>
          </div>
        </Navbar.Collapse>
      </div>
    </Navbar>

  );

}

export default Navigation;