export { default as Navigation } from "./Navigation";
export { default as Footer } from "./Footer";
export { default as Home } from "./Home";
export { default as About } from "./About";
export { default as Connect } from "./Connect";
export { default as Teaching } from "./Teaching";
export { default as Give } from "./Give";