import React from "react";
import img1 from "../assets/acts.png"
import img2 from "../assets/mens_bible_study.jpg"
import img3 from "../assets/proverbs.png"
import img4 from "../assets/womens-bible-study2.png"
import img5 from "../assets/exp_banner.jpg"
import img6 from "../assets/saturday-prayer-2018 (1) copy.jpg"

function ColSection(){
    return(
        <div className="row w-100 m-0 mb-5 h-25">
        <div className="col-md">

            <div className="mt-6">
                <div className="container col-7 justify-content-center">
                    <h1 className="text-center display-6 pb-4 border-bottom mb-5">Visit<br className="d-none d-md-block"/> Us</h1>
                </div>
                <div className="container">
                    <div className="visit-card m-md-4 shadow">
                        <iframe width="100%" height="100%" loading="lazy" title="location" allowFullScreen
                            src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJFdcP5coq3YARqs2aItInCq0&key=AIzaSyCgmR03fmZz4bc-GriUPZTUFl3XFwOLKbE"></iframe>
                    </div>
                </div>
            </div>
        </div>


        <div className="col-md">

            <div className="mt-6">
                <div className="container col-7 justify-content-center">
                    <h1 className="text-center display-6 pb-4 border-bottom mb-5">Upcoming<br className="d-none d-md-block"/> Events</h1>
                </div>
            </div>

            <div className="container">
                <div id="carouselExampleControls" className="carousel slide m-md-4" data-bs-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src={img1} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={img2} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={img3} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={img4} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={img5} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={img6} className="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <i className="lni lni-arrow-left-circle"></i>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <i className="lni lni-arrow-right-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    )
}

export default ColSection;