import React from "react";
import ServiceTimes from "./ServiceTimes"
import Welcome from "./Welcome"
import RecentVideos from './RecentVideos'
import ColSection from "./ColSection";

function Home() {
  return (
    <div className="home">
      <ServiceTimes />
      <Welcome />
      <RecentVideos />
      <ColSection />
    </div>
  );
}

export default Home;